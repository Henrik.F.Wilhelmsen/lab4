package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row, col);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();

        for (int y = 0; y < numberOfRows(); y++) {
            for (int x = 0; x < numberOfColumns(); x++) {
                nextGeneration.set(y,x,getNextCell(y,x));
            }
        }

        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {

        int n = countNeighbors(row,col,CellState.ALIVE);

        CellState state = getCellState(row, col);

        if (state == CellState.ALIVE){
            return CellState.DYING;
        }
        else if (state == CellState.DYING) {
            return CellState.DEAD;
        }
        else if (state == CellState.DEAD && n == 2){
            return CellState.ALIVE;
        } else {
            return CellState.DEAD;
        }
    }

    private int countNeighbors(int row, int col, CellState state) {
        int count = 0;

        int initialy = row;
        int endy = row;
        int initialx = col;
        int endx = col;

        if (row > 0) {
            initialy--;
        }
        if (row < numberOfRows()-1){
            endy++;
        }
        if (col > 0){
            initialx--;
        }
        if (col < numberOfColumns()-1){
            endx++;
        }

        for (int y = initialy ; y <= endy ; y++){
            for (int x = initialx; x <= endx; x++) {
                if (getCellState(y,x) == state && !(y == row && x == col)){
                    count++;
                }
            }
        }

        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}