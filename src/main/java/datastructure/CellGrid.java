package datastructure;

import cellular.CellState;

import java.util.Arrays;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        grid = new CellState[rows][columns];

        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < columns; x++) {
                grid[y][x] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid newGrid = new CellGrid(this.rows,this.cols,CellState.DEAD);

        for (int y = 0; y < this.rows; y++) {
            for (int x = 0; x < this.cols; x++) {
                newGrid.set(y, x, grid[y][x]);
            }

        }

        return newGrid;
    }
    
}
